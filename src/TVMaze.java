import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.*;

public class TVMaze
{
    private static String url = "http://api.tvmaze.com/search/people?q=";
    
    static Scanner in = new Scanner(System.in);
    
    static Map<Integer, Persons> actors = new HashMap();
    
    public static void main(String[] args)
    {
        //Map<Integer, Persons> actors = new HashMap();
        
        loadData();
        showMenu();
        
        boolean end = false;
        int action = 0;
        
        while(end == false)
        {
            try
            {
                System.out.println("\n- Please enter action");
                action = in.nextInt();
                in.nextLine();
                System.out.println("");
            }
            catch(InputMismatchException e)
            {
                System.out.println("- Wrong input detected");
                in.nextLine();
            }
            
            if(action > 0 && action < 8)
            {
                switch(action)
                {
                    case 1:
                        System.out.println("- Please enter search term for Actor");
                        String searchTerm = in.nextLine();
                        
                        searchPerson(searchTerm);
                        break;
                        
                    case 2:
                        System.out.println("- Listing all actors");
                        listAllPersons();
                        break;
                                
                    case 3:
                        System.out.println("- Sorting actors by ID");
                        sortByID();
                        break;
                        
                    case 4:
                        System.out.println("- Sorting actors by Rating");
                        sortByRating();
                        break;
                        
                    case 5:
                        System.out.println("- Please enter the actor's id enable edit");
                        int id = in.nextInt();
                        editPerson(id);
                        break;
                            
                    case 6:
                        System.out.println("- Exporting search results to HTML...");
                        System.out.println("---FUNCTION NOT AVAILABLE---");
                        break;
                        
                    case 7:
                        System.out.println("- Saving data and exitting...");
                        saveData();
                        end = true;
                        break;
                }
            }
            else
            {
                System.out.println("- No action for this option is available");
            }
        }
    }
    
    public static void searchPerson(String searchTerm)
    {
        Set<Integer> keys = actors.keySet();
        Iterator<Integer> iter = keys.iterator();
        Persons p = new Persons();
        
        System.out.println("- From the local database: \n");
        while(iter.hasNext())
        {
            int key = iter.next();
            p = actors.get(key);
            
            if(p.getName().toLowerCase().indexOf(searchTerm.toLowerCase()) > -1)
            {
                p.display();
                System.out.println("");
            }
        }
        
        System.out.println("- From the TV Maze database");
        try 
        {
            String encode = URLEncoder.encode(searchTerm, "UTF-8");
            URL website = new URL(url+encode);
            InputStream siteIn = website.openStream();
            JsonReader reader = Json.createReader(siteIn);
            JsonArray allResults = reader.readArray();
            
            System.out.println("");
            
            boolean new_actor_found = false;
            
            for(int i = 0; i < allResults.size(); i++)
            {
                JsonObject obj = allResults.getJsonObject(i);
                JsonObject person = obj.getJsonObject("person");
                
                int id = person.getInt("id");
                
                //if the actor doesnt exist in the local database, query all data and add to the database
                if(!actors.containsKey(id))
                {
                    String title = person.getString("name");

                    JsonNumber score = obj.getJsonNumber("score");
                    double scoreValue = Double.parseDouble(score.toString());

                    String mediumURL = "";
                    String originalURL = "";

                    if(!person.isNull("image"))
                    {
                        JsonObject imageURLs = person.getJsonObject("image");
                        mediumURL = imageURLs.getString("medium");
                        originalURL = imageURLs.getString("original");
                    }
                    else
                    {
                        mediumURL = "";
                        originalURL = "";
                    }
                    String[] urls = {mediumURL, originalURL};

                    JsonObject links = person.getJsonObject("_links");
                    JsonObject self = links.getJsonObject("self");
                    String href = self.getString("href");

                    Persons per = new Persons(title, id, scoreValue, urls, href, 0, "", searchTerm);

                    actors.put(id, per);
                    per.display();
                    System.out.println("");
                    new_actor_found = true;
                }
            }
            System.out.println(new_actor_found ? "" : "- No new actors being found from TV Maze");
        }
        catch (IOException ex) 
        {
            Logger.getLogger(TVMaze.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void loadData()
    {
        try
        {
            DataInputStream dis = new DataInputStream(new FileInputStream("Actors.dat"));
            
            int count = 0;
            
            while(dis.available() > 0)
            {
                String p_name = dis.readUTF();
                int p_id = dis.readInt();
                double p_score = dis.readDouble();
                String p_m_img = dis.readUTF();
                String p_o_img = dis.readUTF();
                String p_link = dis.readUTF();
                double p_rating = dis.readDouble();
                String p_comment = dis.readUTF();
                String p_search_term = dis.readUTF();
                
                String[] urls = {p_m_img, p_o_img};
                
                Persons new_person = new Persons(p_name, p_id, p_score, urls, p_link, p_rating, p_comment, p_search_term);
                actors.put(new_person.getId(), new_person);
                count++;
            }
                
            dis.close();
            
            System.out.println("- Number of actors loaded: " + count + "\n");
        }
        catch(IOException e)
        {
            
        }
    }
    
    public static void saveData()
    {
        Set<Integer> keys = actors.keySet();
        Iterator<Integer> iter = keys.iterator();
        Persons p = new Persons();
        
        try
        {
            DataOutputStream dos = new DataOutputStream(new FileOutputStream("Actors.dat"));

            int count = 0;
            
            while(iter.hasNext())
            {
                int key = iter.next();
                p = actors.get(key);

                try
                {
                    dos.writeUTF(p.getName());
                    dos.writeInt(p.getId());
                    dos.writeDouble(p.getRating());
                    dos.writeUTF(p.getImageUrls()[0]);
                    dos.writeUTF(p.getImageUrls()[1]);
                    dos.writeUTF(p.getPersonLink());
                    dos.writeDouble(p.getMyRating());
                    dos.writeUTF(p.getMyComment());
                    dos.writeUTF(p.getSearchTerm());
                }
                catch(IOException e)
                {

                }
                count++;
            }
            System.out.println("- Actors in the database: " + count);
            dos.close();
        }
        catch(IOException e)
        {
            
        }
    }
    
    public static void listAllPersons()
    {
        Set<Integer> keys = actors.keySet();
        Iterator<Integer> iter = keys.iterator();
        Persons p = new Persons();
        
        System.out.println("- All actors in the database: \n");
        while(iter.hasNext())
        {
            int key = iter.next();
            p = actors.get(key);
            p.display();
            System.out.println("");
        }
    }
    
    public static void editPerson(int id)
    {
        if(actors.containsKey(id))
        {
            Persons p = actors.get(id);

            System.out.println("- Enter: ");
            System.out.println("- 1 to edit the rating");
            System.out.println("- 2 to edit the comments");
            System.out.println("- 3 to exit");

            int action = 0;
            boolean end = false;

            while(end == false)
            {
                try
                {
                    System.out.println("- Please enter action");
                    action = in.nextInt();
                    in.nextLine();
                }
                catch(InputMismatchException e)
                {
                    System.out.println("- Wrong input detected");
                    in.nextLine();
                }

                if(action > 0 && action < 4)
                {
                    switch(action)
                    {
                        case 1:
                            System.out.print("- Please enter the new rating: ");
                            double rating = in.nextDouble();
                            p.setMyRating(rating);
                            break;

                        case 2:
                            System.out.print("- Please enter the new comments: ");
                            String comment = in.nextLine();
                            p.setMyComment(comment);
                            break;

                        case 3:
                            end = true;
                            System.out.println("-Exitting edit interface...");
                            break;
                    }
                }
                else
                {
                    System.out.println("- Wrong input!");
                }
            }
        }
        else
        {
            System.out.println("- No actors with this ID exist in the local database.");
        }
    }
    
    public static void showMenu()
    {
        System.out.println("- Enter:");
        System.out.println("- 1 to search for actors.");
        System.out.println("- 2 to list all actors");
        System.out.println("- 3 to sort actors by ID");
        System.out.println("- 4 to sort actors by Rating");
        System.out.println("- 5 to edit actor's details");
        System.out.println("- 6 to export search results to HTML ---NOT AVAILABLE---");
        System.out.println("- 7 to save and exit");
    }
    
    public static void sortByID()
    {
        Set<Integer> keys = actors.keySet();
        Iterator<Integer> iter = keys.iterator();
        Persons p = new Persons();
        
        ArrayList<Persons> actorList = new ArrayList();
        
        System.out.println("- All actors in the database: \n");
        while(iter.hasNext())
        {
            int key = iter.next();
            p = actors.get(key);
            actorList.add(p);
            Collections.sort(actorList, new IdComparator());
        }
        
        for(Persons ac: actorList)
        {
            ac.display();
            System.out.println("");
        }
    }
    
    public static void sortByRating()
    {
        Set<Integer> keys = actors.keySet();
        Iterator<Integer> iter = keys.iterator();
        Persons p = new Persons();
        
        ArrayList<Persons> actorList = new ArrayList();
        
        System.out.println("- All actors in the database: \n");
        while(iter.hasNext())
        {
            int key = iter.next();
            p = actors.get(key);
            actorList.add(p);
            Collections.sort(actorList, new RatingComparator());
        }
        
        for(Persons ac: actorList)
        {
            ac.display();
            System.out.println("");
        }
    }
}