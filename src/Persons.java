import java.util.Arrays;
import java.util.Objects;

public class Persons 
{
    private String name;
    private int id;
    private double rating;
    private String[] imageUrls;
    private String personLink;
    private double myRating;
    private String myComment;
    private String searchTerm;
    
    public Persons() 
    {
        this.name = "";
        this.id = 0;
        this.rating = 0;
        this.imageUrls = null;
        this.personLink = "";
        this.myRating = 0;
        this.myComment = "";
        this.searchTerm = searchTerm;
    }

    public Persons(String name, int id, double score, String imageUrls[], String personLink, double myRating, String myComment, String searchTerm) 
    {
        this.name = name;
        this.id = id;
        this.rating = score;
        this.imageUrls = imageUrls;
        this.personLink = personLink;
        this.myRating = myRating;
        this.myComment = myComment;
        this.searchTerm = searchTerm;
    }

    public String getName() 
    {
        return name;
    }

    public void setName(String name) 
    {
        this.name = name;
    }

    public int getId() 
    {
        return id;
    }

    public void setId(int id) 
    {
        this.id = id;
    }

    public double getRating() 
    {
        return rating;
    }

    public void setRating(double rating) 
    {
        this.rating = rating;
    }
    
    public String[] getImageUrls() 
    {
        return imageUrls;
    }

    public void setImageUrls(String[] imageUrls) 
    {
        this.imageUrls = imageUrls;
    }

    public String getPersonLink() 
    {
        return personLink;
    }

    public void setPersonLink(String personLink) 
    {
        this.personLink = personLink;
    }

    public double getMyRating() 
    {
        return myRating;
    }

    public void setMyRating(double myRating) 
    {
        this.myRating = myRating;
    }

    public String getMyComment() 
    {
        return myComment;
    }

    public void setMyComment(String myComment) 
    {
        this.myComment = myComment;
    }

    public String getSearchTerm() 
    {
        return searchTerm;
    }

    public void setSearchTerm(String searchTerm) 
    {
        this.searchTerm = searchTerm;
    }

    @Override
    public int hashCode() 
    {
        int hash = 3;
        hash = 59 * hash + Objects.hashCode(this.name);
        hash = 59 * hash + this.id;
        hash = 59 * hash + (int) (Double.doubleToLongBits(this.rating) ^ (Double.doubleToLongBits(this.rating) >>> 32));
        hash = 59 * hash + Arrays.deepHashCode(this.imageUrls);
        hash = 59 * hash + Objects.hashCode(this.personLink);
        hash = 59 * hash + (int) (Double.doubleToLongBits(this.myRating) ^ (Double.doubleToLongBits(this.myRating) >>> 32));
        hash = 59 * hash + Objects.hashCode(this.myComment);
        return hash;
    }

    @Override
    public boolean equals(Object obj) 
    {
        if (obj == null) 
        {
            return false;
        }
        if (getClass() != obj.getClass()) 
        {
            return false;
        }
        final Persons other = (Persons) obj;
        if (!Objects.equals(this.name, other.name)) 
        {
            return false;
        }
        if (this.id != other.id) 
        {
            return false;
        }
        if (Double.doubleToLongBits(this.rating) != Double.doubleToLongBits(other.rating)) 
        {
            return false;
        }
        if (!Arrays.deepEquals(this.imageUrls, other.imageUrls)) 
        {
            return false;
        }
        if (!Objects.equals(this.personLink, other.personLink)) 
        {
            return false;
        }
        if (Double.doubleToLongBits(this.myRating) != Double.doubleToLongBits(other.myRating)) 
        {
            return false;
        }
        if (!Objects.equals(this.myComment, other.myComment)) 
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString() 
    {
        return "Persons{" + "name=" + name + ", id=" + id + ", score=" + rating + ", imageUrls=" + imageUrls + ", personLink=" + personLink + ", myRating=" + myRating + ", myComment=" + myComment + '}';
    }
    
    public void display()
    {
        System.out.println("Name: " + name);
        System.out.println("ID: " + id);
        System.out.println("Rating: " + rating);
        System.out.println("Medium URL: " + imageUrls[0]);
        System.out.println("Original URL: " + imageUrls[1]);
        System.out.println("Link: " + personLink);
        System.out.println("My Rating: " + myRating);
        System.out.println("Comments: " + myComment);
        System.out.println("Term used to search this actor: " + searchTerm);
    }
}