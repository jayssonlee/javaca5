import java.util.Comparator;

public class IdComparator implements Comparator<Persons> 
{
    @Override
    public int compare(Persons o1, Persons o2) 
    {
        
        return (int)(o1.getId()- o2.getId());
    }
}
