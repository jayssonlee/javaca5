import java.util.Comparator;

public class RatingComparator implements Comparator<Persons> 
{
    @Override
    public int compare(Persons o1, Persons o2) 
    {
        
        return (int)(o1.getRating()- o2.getRating());
    }
}
